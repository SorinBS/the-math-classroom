import { Component, OnInit } from '@angular/core';
import { MathLibSbAngularService } from 'math-lib-sb-angular';

import * as mathLib from 'math-lib-ts-sb';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'the-math-classroom';

  constructor(private mathLibSbAngularService: MathLibSbAngularService) {}

  ngOnInit(): void {
    console.log(mathLib.truncateNumberWithoutRounding('100.455'));
    
    console.log(this.mathLibSbAngularService.truncateNumberWithoutRounding('100.455'));
  }
}
